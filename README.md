# Steps for installation

* Run the `npm install` command after cloning this repo
* Run the `gulp` command for the live sync
* Run the `gulp build` to generate the dist folder

# Folder use

* All work should be done in the "app" folder
* The "SCSS" folder should be used for editing styles
* The "CSS" folder is the resulting SCSS files
* Include all CSS and JS files on the index.html to preview the changes